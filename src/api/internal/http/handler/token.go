package handler

import (
	"fmt"
	"net/http"
	"strconv"
)

// GetGoogleToken - GET auth/google/token
func GetGoogleToken(res http.ResponseWriter, req *http.Request) {
	fmt.Println("Hello from Get Google Token")
	for key, value := range req.URL.Query() {
		for index, element := range value {
			res.Write([]byte(key + "[" + strconv.Itoa(index) + "]: " + element))
		}
	}
}
