package route

import (
	"github.com/gorilla/mux"
	"gitlab.com/go19/gochatapp/src/api/internal/http/handler"
)

// GetRouter - Get Api Router
func GetRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/auth/google/token", handler.GetGoogleToken).
		Methods("Get")
	return r
}
