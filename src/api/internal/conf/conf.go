package conf

// Config - Api Configuration
type Config struct {
	ListenAddress string
}

// LoadConfig - Load Api Configuration
func LoadConfig() Config {
	return Config{
		ListenAddress: ":80",
	}
}
