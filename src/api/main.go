package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/go19/gochatapp/src/api/internal/conf"
	"gitlab.com/go19/gochatapp/src/api/internal/http/route"
)

func main() {
	conf := conf.LoadConfig()
	fmt.Println("Configuration loaded listening at " + conf.ListenAddress)
	log.Fatal(http.ListenAndServe(":80", route.GetRouter()))
}
